#include <stdio.h>
#include <string.h>
#include "md5.h"

int main(int argc, char *argv[])
{
	FILE *fp1;
	FILE *fp2;
	char inString[1000];
	char *outString;
	int strLength=0;

	// Process if two filename arguments are provided
	if (argc > 2) {
	    // Open the first source file for read-only		
		fp1 = fopen(argv[1], "r");
		
		if (fp1 == NULL) {
			// Invalid source filename used
			printf("Could not open the source file %s\n",argv[1]);
			return 1;
		}
		else {
	        // Open the second target file for writing		
			fp2 = fopen(argv[2], "w");					

			// Valid source filename, read one line at a time			
			while (fgets(inString, sizeof inString, fp1) != NULL) {
				// TEST ONLY ... printf("inString value is %s", inString);
				strLength=strlen(inString)-1;
				outString=md5(inString, strLength);
				// TEST ONLY ... fprintf(fp2, "%d %s\n", i++, outString);				
				fprintf(fp2, "%s\n", outString);
			}
			fclose(fp1);
			fclose(fp2);			
			return 0;
		}
	}
	// If there is only one filename, output to stdout
	else if (argc == 2) {
	    // Open the source file for read-only		
		fp1 = fopen(argv[1], "r");
		
		if (fp1 == NULL) {
			// Invalid source filename used
			printf("Could not open the source file %s\n",argv[1]);
			return 1;
		}
		else {
			// Valid source filename, read one line at a time			
			while (fgets(inString, sizeof inString, fp1) != NULL) {
				// TEST ONLY ... printf("inString value is %s", inString);	
				strLength=strlen(inString)-1;
				outString=md5(inString, strLength);
				// TEST ONLY ... printf("%d %s\n", i++, outString);
				printf("%s\n", outString);
			}
			fclose(fp1);
			return 0;
		}		
	}
	// Missing filename arguments	
	else {
		printf("usage1: program2 <filename1>\n");
		printf("  Only one filename, the output will be stdout2.\n");
		printf("  Filename may include full path, i.e. c:\\temp\\file.txt\n");		
		printf("usage2: program2 <filename1> <filename2>\n");
		printf("  Second filename is the output.\n");
		return 1;
	}
}